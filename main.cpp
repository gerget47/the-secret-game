#include <SFML/Graphics.hpp>
#include "view.h" //Добавляем камеру
#include "map.h"

using namespace sf;
int k = 0;

bool check_endMap() {
    for (int i = 0; i < H; i ++)
        for (int j = 0; j < W; j ++)
        {
            if (i == 0) return true;
            else if (i > 0 and i != H - 1 and  j == 0) return true;
            else if (i == H - 1) return true;
            else if (j == W - 1 and i > 0 and i != H - 1) return true;
        }
}
class PLAYER
{
public:
    float dx, dy;
    FloatRect rect;
    Sprite sprite;
    float currentFrame;
    PLAYER(Texture &image)
    {
        sprite.setTexture(image);
//        rect = FloatRect(200, 265, 190, 260); //250 100 100 100
        dx=dy=0;
        currentFrame = 0;
    }

    void update(float time) {
        sprite.setTextureRect(IntRect(0, 0, 90, 120));

        rect.left += dx * time;
        rect.top += dy * time;
        currentFrame += 0.002 * time; //0.002


        if (currentFrame > 3) currentFrame -= 3; //3

        if (dx > 0) sprite.setTextureRect(IntRect(250 * int(currentFrame), 130,100, 110));
        if (dx < 0) sprite.setTextureRect(IntRect(250 * int(currentFrame) + 220, 130, -120, 110));


        sprite.setPosition(rect.left, rect.top);

        dx = dy = 0;
    }

    float GetPlayerCoordinateX() {
        return rect.left;
    }
    float GetPlayerCoordinateY() {
        return rect.top;
    }
};

int main() {
    int width = 1280, height = 720;

    RenderWindow window(VideoMode(width, height), "Test"); //Строка для создания окна
    view.reset(FloatRect(0, 0, width, height));

    RectangleShape rectangle;
    Texture image, texture, t_map; //Создание первой текстуры
    Sprite s, s2, s3_map; //Создание спрайтов

    image.loadFromFile("/Users/nikitasvetkin/CLionProjects/SFML/Figure2.1.png"); //Загрузка изображения в переменную текстуры
    texture.loadFromFile("/Users/nikitasvetkin/CLionProjects/SFML/DoubleTexture.jpg"); //Загрузка текстур
    t_map.loadFromFile("/Users/nikitasvetkin/CLionProjects/SFML/map.png");
    s2.setTexture(texture); //Установка текстуры в спрайт
    s3_map.setTexture(t_map);

    PLAYER p(image); // Создание объекта в классе

    Clock clock; // Создание переменной время

    while (window.isOpen()) {
        float time = clock.getElapsedTime().asMicroseconds(); // создание переменной прощедщего времении в милисекундах
        clock.restart(); // Запуск времени
        time = time / 300; // Настройка времени
        Event event;
        while (window.pollEvent(event)) {
            if (event.type == Event::Closed ||
                (event.type == Event::KeyPressed && event.key.code == Keyboard::Enter))
                window.close();
        }
        if (Keyboard::isKeyPressed(Keyboard::A)) {
            p.dx = -0.1;
            getplayercoordinateforview(p.GetPlayerCoordinateX(),p.GetPlayerCoordinateY());
        }
        if (Keyboard::isKeyPressed(Keyboard::D)) {
            p.dx = 0.1;
            getplayercoordinateforview(p.GetPlayerCoordinateX(),p.GetPlayerCoordinateY());
        }
        if (Keyboard::isKeyPressed(Keyboard::S)) {
            p.dy = 0.1;
            getplayercoordinateforview(p.GetPlayerCoordinateX(),p.GetPlayerCoordinateY());
        }
        if (Keyboard::isKeyPressed(Keyboard::W)) {
            p.dy = -0.1;

            getplayercoordinateforview(p.GetPlayerCoordinateX(),p.GetPlayerCoordinateY());
        }

        viewmap(time);
        p.update(time);
        window.setView(view);
        window.clear(Color::White);

        float py, px;
        py = p.sprite.getPosition().y; px = p.sprite.getPosition().x;
        // Коллизия
        for (int i = py / 32; i < (py / 32) + 1; i++)
            for (int j = px / 32; j < (px / 32) + 3; j++) {
                if (Map[i + 2][j] == 'B') {
                    int i1 = i + 2;
                    if ((i1 * 32) - py < 96 and
                             (i1 * 32 - py) > 0 and Map[i1 - 1][j + 1] != 'B' and Map[i1 - 1][j - 1] != 'B') {
                        p.rect.top = p.rect.top - 1;
                    }
                }
                if (Map[i][j] == 'B') {
                    if ((i * 32) != py and py - (i) * 32 < 96 and
                            0 < py - (i) * 32 and Map[i + 1][j + 1] != 'B' and Map[i + 1][j - 1] != 'B') {
                        p.rect.top = p.rect.top + 1;
                    }
                    else if ((j * 32) != px and px - (j) * 32 < 20 and
                        -32 < px - (j) * 32) {
                        p.rect.left = p.rect.left + 1;
                    }
                    else if ((j * 32) != px and (j) * 32 - px < 96 and
                        0 < (j) * 32 - px) {
                        p.rect.left = p.rect.left - 1;
                    }
                }
            }
        for (int i = 0; i < H + 1; i ++) {
            for (int j = 0; j < W + 1; j ++) {
                if (Map[i][j] == 'B' or Map[i][j] == 'b') {
                    s3_map.setTextureRect(IntRect(132, 66, 34,34)); //132 66
                    s3_map.setPosition(j * 32, i * 32);
//                    s2.setTextureRect(IntRect(0 + 32,0,32,32));
//                    s2.setPosition(j * 32, i * 32);
                    window.draw(s3_map);
                    continue;
                }
                if (Map[i][j] == '0') {
                    rectangle.setFillColor(Color::Red);
                    rectangle.setPosition(j * 32, i * 32);
                    rectangle.setSize(Vector2f(32, 32));// 48 74
                    window.draw(rectangle);
                    continue;
                }
                if (Map[i][j] == ' ') {
                    s3_map.setTextureRect(IntRect(32, 32, 34,34)); //132 66
                    s3_map.setPosition(j * 32, i * 32);
//                    s2.setTextureRect(IntRect(0,0,32,32));
//                    s2.setPosition(j * 32, i * 32);
                    window.draw(s3_map);
                    s3_map.setTextureRect(IntRect(65, 190, 34,34)); //132 66
                    s3_map.setPosition(j * 32, i * 32);
                    window.draw(s3_map);
                }
            }
        }
        window.draw(p.sprite);
        window.display();
    }
}
